package com.heima.search.test;

import java.util.Calendar;

/**
 * ClassName: InstantTest
 * Package: com.heima.search.test
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/5 15:48
 * @Version 11
 */
public class InstantTest {
    public static void main(String[] args) {
        // 获取当前时间
        Calendar currentCalendar = Calendar.getInstance();

        // 计算二十年后的时间
        Calendar futureCalendar = (Calendar) currentCalendar.clone();
        futureCalendar.add(Calendar.YEAR, 40);

        // 获取时间戳
        long currentTimestamp = currentCalendar.getTimeInMillis() / 1000;
        long futureTimestamp = futureCalendar.getTimeInMillis() / 1000;

        // 打印结果
        System.out.println("当前时间戳: " + currentTimestamp);
        System.out.println("四十年后的时间戳: " + futureTimestamp);
    }
}
