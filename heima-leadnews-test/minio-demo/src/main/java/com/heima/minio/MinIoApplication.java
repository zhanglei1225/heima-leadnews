package com.heima.minio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ClassName: MinIoApplication
 * Package: com.heima.minio
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/25 16:13
 * @Version 11
 */
@SpringBootApplication
public class MinIoApplication {
    public static void main(String[] args) {
        SpringApplication.run(MinIoApplication.class,args);
    }
}
