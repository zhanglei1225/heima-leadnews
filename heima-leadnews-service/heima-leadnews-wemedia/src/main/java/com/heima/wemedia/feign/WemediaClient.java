package com.heima.wemedia.feign;

import com.heima.apis.wemedia.IWemediaClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.wemedia.service.WmChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * ClassName: WemediaClient
 * Package: com.heima.wemedia.feign
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/7 11:19
 * @Version 11
 */
@RestController
public class WemediaClient implements IWemediaClient {

    @Autowired
    private WmChannelService wmChannelService;

    @GetMapping("/api/v1/channel/list")
    public ResponseResult<List<WmChannel>> getChannels() {
        return wmChannelService.findAll();
    }
}
