package com.heima.hystrix.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: HelloController
 * Package: com.heima.hystrix.controller
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/30 17:03
 * @Version 11
 */
@RestController
public class HelloController {

    @GetMapping("/hello/{id}")
    @HystrixCommand
    public String hello(@PathVariable("id") Integer id){
        if (id==0) {
            throw new RuntimeException("服务出错");
        }else{
            return "hello";
        }
    }
    /*(fallbackMethod = "queryOrderByUserId_fallback",commandProperties = {
            //设置Hystrix的超时时间，默认1s
            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value = "3000"),
            //监控时间 默认5000 毫秒
            @HystrixProperty(name="circuitBreaker.sleepWindowInMilliseconds",value = "5000"),
            //失败次数。默认20次
            @HystrixProperty(name="circuitBreaker.requestVolumeThreshold",value = "5"),
            //失败率 默认50%
            @HystrixProperty(name="circuitBreaker.errorThresholdPercentage",value = "50")

    })*/
}
