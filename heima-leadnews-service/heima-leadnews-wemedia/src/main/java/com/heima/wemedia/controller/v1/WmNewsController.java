package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsDto1;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.wemedia.service.WmNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: WmNewsController
 * Package: com.heima.wemedia.controller.v1
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 18:47
 * @Version 11
 */
@RestController
@RequestMapping("/api/v1/news")
public class WmNewsController {

    @Autowired
    private WmNewsService wmNewsService;

    /**
     * 查询文章
     * @param dto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult findAll(@RequestBody WmNewsPageReqDto dto){
        return wmNewsService.findAll(dto);
    }

    /**
     * 文章发布
     * @param dto
     * @return
     */
    @PostMapping("/submit")
    public ResponseResult submitNews(@RequestBody WmNewsDto dto){
        return wmNewsService.submitNews(dto);
    }

    /**
     * 查看详情
     * @param id
     * @return
     */
    @GetMapping("/one/{id}")
    public ResponseResult showDetails(@PathVariable Integer id){
        return wmNewsService.showDetails(id);
    }

    /**
     * 文章删除
     * @param id
     * @return
     */
    @GetMapping("del_news/{id}")
    public ResponseResult deleteNews(@PathVariable Integer id){
        return wmNewsService.deleteNews(id);
    }

    /**
     * 文章上下架
     * @param wmNewsDto1
     * @return
     */
    @PostMapping("/down_or_up")
    public ResponseResult downOrUp(@RequestBody WmNewsDto1 wmNewsDto1){
        return wmNewsService.downOrUp(wmNewsDto1);
    }
}
