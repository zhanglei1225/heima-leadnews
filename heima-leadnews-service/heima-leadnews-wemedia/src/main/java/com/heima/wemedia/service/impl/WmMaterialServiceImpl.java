package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.constants.WemediaConstants;
import com.heima.file.service.FileStorageService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * ClassName: WmMaterialServiceImpl
 * Package: com.heima.wemedia.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 10:40
 * @Version 11
 */
@Service
@Slf4j
@Transactional
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialMapper, WmMaterial> implements WmMaterialService {

    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private WmNewsMaterialMapper wmNewsMaterialMapper;

    /**
     * 素材上传
     *
     * @param multipartFile
     * @return
     */
    @Override
    public ResponseResult uploadPicture(MultipartFile multipartFile) {
        // 参数校验
        if (multipartFile == null || multipartFile.getSize() == 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        // 登录用户校验
        if (WmThreadLocalUtil.getUser() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        // 上传图片到minio中
        String filename = UUID.randomUUID().toString().replace("-", "");
        String originalFilename = multipartFile.getOriginalFilename();
        String postfix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String path = null;
        try {
            path = fileStorageService.uploadImgFile("", filename + postfix, multipartFile.getInputStream());
            log.info("素材上传成功，url:{}", path);
        } catch (IOException e) {
            log.error("素材上传失败,{}", e.getMessage());
        }
        // 保存到数据库中
        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setIsCollection((short) 0);
        wmMaterial.setType((short) 0);
        wmMaterial.setUrl(path);
        wmMaterial.setCreatedTime(new Date());
        wmMaterial.setUserId(WmThreadLocalUtil.getUser().getId());
        save(wmMaterial);
        // 返回结果
        return ResponseResult.okResult(wmMaterial);
    }

    /**
     * 素材列表查询
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findList(WmMaterialDto dto) {
        // 1.检查参数
        dto.checkParam();
        // 用户校验
        if (WmThreadLocalUtil.getUser() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        // 2.分页查询
        IPage page = new Page(dto.getPage(), dto.getSize());
        LambdaQueryWrapper<WmMaterial> wrapper = new LambdaQueryWrapper<>();
        // 是否收藏
        if (dto.getIsCollection() != null && dto.getIsCollection().equals(WemediaConstants.COLLECT_MATERIAL)) {
            wrapper.eq(WmMaterial::getIsCollection, dto.getIsCollection());
        }
        // 根据用户展示信息
        if (WmThreadLocalUtil.getUser() != null) {
            Integer userId = WmThreadLocalUtil.getUser().getId();
            wrapper.eq(WmMaterial::getUserId, userId);
        }
        // 上传时间倒序排序
        wrapper.orderByDesc(WmMaterial::getCreatedTime);
        page = page(page, wrapper);
        // 3.结果返回
        PageResponseResult pageResponseResult = new PageResponseResult(dto.getPage(), dto.getSize(), (int) page.getTotal());
        pageResponseResult.setData(page.getRecords());
        return pageResponseResult;
    }

    // 图片删除
    @Override
    public ResponseResult delPicture(Integer id) {
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        if (getById(id)==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        WmNewsMaterial wmNewsMaterial = wmNewsMaterialMapper.selectOne(Wrappers.<WmNewsMaterial>lambdaQuery().eq(WmNewsMaterial::getMaterialId, id));
        if (wmNewsMaterial!=null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"文件删除失败");
        }
        removeById(id);
        return ResponseResult.errorResult(AppHttpCodeEnum.SUCCESS);
    }

    // 素材收藏或取消
    @Override
    public ResponseResult cancelOrCollect(Integer id, short isCollection) {
        WmMaterial wmMaterial = getById(id);
        if (wmMaterial==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        wmMaterial.setIsCollection(isCollection);
        updateById(wmMaterial);
        return ResponseResult.errorResult(AppHttpCodeEnum.SUCCESS);
    }
}
