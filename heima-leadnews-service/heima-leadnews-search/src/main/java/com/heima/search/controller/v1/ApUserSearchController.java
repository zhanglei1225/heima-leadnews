package com.heima.search.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.search.pojos.ApUserSearch;
import com.heima.search.service.ApUserSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: ApUserSearchController
 * Package: com.heima.search.controller.v1
 * Description: APP用户搜索信息表 前端控制器
 *
 * @Author Tree
 * @Create 2023/7/6 11:32
 * @Version 11
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/history")
public class ApUserSearchController {

    @Autowired
    private ApUserSearchService apUserSearchService;

    @PostMapping("/load")
    public ResponseResult findUserSearch(){
        return apUserSearchService.findUserSearch();
    }
    @PostMapping("/del")
    public ResponseResult delUserSearch(@RequestBody ApUserSearch apUserSearch){
        return apUserSearchService.delUserSearch(apUserSearch);
    }
}
