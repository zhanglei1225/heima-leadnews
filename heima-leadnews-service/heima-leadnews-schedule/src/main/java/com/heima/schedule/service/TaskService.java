package com.heima.schedule.service;

/**
 * ClassName: TaskService
 * Package: com.heima.schedule.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/2 16:58
 * @Version 11
 */

import com.heima.model.schedule.dtos.Task;

/**
 * 对外访问接口
 */
public interface TaskService {
    /**
     * 添加任务
     * @param task
     * @return
     */
    long addTsk(Task task);

    /**
     * 取消任务
     * @param taskId
     * @return
     */
    boolean cancelTask(long taskId);

    /**
     * 按照类型和优先级来拉取任务
     * @param type
     * @param priority
     * @return
     */
    Task poll(int type,int priority);
}
