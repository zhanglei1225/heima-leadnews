package com.heima.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ClassName: KafkaDemoApplication
 * Package: com.heima.kafka
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/9 17:09
 * @Version 11
 */
@SpringBootApplication
public class KafkaDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(KafkaDemoApplication.class,args);
    }
}
