package com.heima.model.admin.dtos;

import lombok.Data;

/**
 * ClassName: ChannelDto
 * Package: com.heima.model.admin.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 21:18
 * @Version 11
 */
@Data
public class ChannelDto {

    // 频道名称
    private String name;

    // 当前页
    private Integer page;

    // 每页显示条数
    private Integer size;
}
