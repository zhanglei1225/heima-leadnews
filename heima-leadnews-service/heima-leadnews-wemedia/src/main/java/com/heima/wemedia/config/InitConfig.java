package com.heima.wemedia.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * ClassName: InitConfig
 * Package: com.heima.wemedia.config
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/30 14:56
 * @Version 11
 */
@Configuration
@ComponentScan("com.heima.apis.article.fallback")
public class InitConfig {
}
