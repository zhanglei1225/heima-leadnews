package com.heima.behavior;

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * ClassName: BehaviorApplication
 * Package: com.heima.behavior
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 9:48
 * @Version 11
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class BehaviorApplication {
    public static void main(String[] args) {
        SpringApplication.run(BehaviorApplication.class,args);
    }

    public class JacksonConfiguration {
        @Bean
        public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {

            return jacksonObjectMapperBuilder -> jacksonObjectMapperBuilder
                    .serializerByType(Long.class, ToStringSerializer.instance)
                    .serializerByType(Long.TYPE, ToStringSerializer.instance);

        }
    }
}
