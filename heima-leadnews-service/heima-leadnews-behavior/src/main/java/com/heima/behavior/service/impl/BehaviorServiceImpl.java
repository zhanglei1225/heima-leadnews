package com.heima.behavior.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.behavior.service.BehaviorService;
import com.heima.common.constants.BehaviorConstants;
import com.heima.common.constants.HotArticleConstants;
import com.heima.common.redis.CacheService;
import com.heima.model.behavior.dtos.CollectionBehaviorDto;
import com.heima.model.behavior.dtos.LikesBehaviorDto;
import com.heima.model.behavior.dtos.ReadBehaviorDto;
import com.heima.model.behavior.dtos.UnLikesBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.UpdateArticleMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.thread.AppThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * ClassName: BehaviorService
 * Package: com.heima.behavior.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 10:16
 * @Version 11
 */
@Service
public class BehaviorServiceImpl implements BehaviorService {
    @Autowired
    private CacheService cacheService;
    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    /**
     * 点赞与取消点赞操作
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult likeBehavior(LikesBehaviorDto dto) {
        // 参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        ApUser user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        // 构造发送消息的对象
        UpdateArticleMess updateArticleMess = new UpdateArticleMess();
        updateArticleMess.setArticleId(dto.getArticleId());
        updateArticleMess.setType(UpdateArticleMess.UpdateArticleType.LIKES);

        String key = BehaviorConstants.LIKE_BEHAVIOR + dto.getArticleId();
        if (dto.getOperation() == 0) { // 点赞
            // 判断是否网络延迟
            if (cacheService.hGet(key, user.getId() + "") != null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST);
            }
            cacheService.hPut(key, user.getId() + "", JSON.toJSONString(dto));
            updateArticleMess.setAdd(1);
        } else { // 取消点赞
            updateArticleMess.setAdd(-1);
            cacheService.hDelete(key, user.getId()+ "");
        }
        //发送消息，数据聚合
        kafkaTemplate.send(HotArticleConstants.HOT_ARTICLE_SCORE_TOPIC,JSON.toJSONString(updateArticleMess));

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 记录阅读行为
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult readBehavior(ReadBehaviorDto dto) {
        // 参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        ApUser user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        // 构造发送消息的对象
        UpdateArticleMess updateArticleMess = new UpdateArticleMess();
        updateArticleMess.setArticleId(dto.getArticleId());
        updateArticleMess.setType(UpdateArticleMess.UpdateArticleType.VIEWS);

        // 如果首次阅读，直接保存信息
        String key =BehaviorConstants.READ_BEHAVIOR+dto.getArticleId();
        if (cacheService.hGet(key,user.getId() + "")==null) {
            cacheService.hPut(key,user.getId() + "",JSON.toJSONString(dto));
            updateArticleMess.setAdd(1);
            // 如果非首次阅读，则将阅读次数加一
        }else {
            dto.setCount(dto.getCount()+1);
            cacheService.hPut(key,user.getId() + "",JSON.toJSONString(dto));
            updateArticleMess.setAdd(-1);
        }
        kafkaTemplate.send(HotArticleConstants.HOT_ARTICLE_SCORE_TOPIC,JSON.toJSONString(updateArticleMess));
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 收藏与取消收藏
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult collectioBehavior(CollectionBehaviorDto dto) {
        // 参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        ApUser user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        // 构造发送消息的对象
        UpdateArticleMess updateArticleMess = new UpdateArticleMess();
        updateArticleMess.setArticleId(dto.getEntryId());
        updateArticleMess.setType(UpdateArticleMess.UpdateArticleType.COLLECTION);

        String key = BehaviorConstants.COLLECTION_BEHAVIOR + dto.getEntryId();
        if (dto.getOperation() == 0) { // 收藏
            // 判断是否网络延迟
            if (cacheService.hGet(key, user.getId() + "") != null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST);
            }
            cacheService.hPut(key, user.getId() + "", JSON.toJSONString(dto));
            updateArticleMess.setAdd(1);
        } else { // 取消收藏
            cacheService.hDelete(key, user.getId()+ "");
            updateArticleMess.setAdd(-1);
        }
        kafkaTemplate.send(HotArticleConstants.HOT_ARTICLE_SCORE_TOPIC,JSON.toJSONString(updateArticleMess));
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 不喜欢与取消不喜欢
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult unLikesBehavior(UnLikesBehaviorDto dto) {
        // 参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        ApUser user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        String key = BehaviorConstants.UNLIKES_BEHAVIOR + dto.getArticleId();
        if (dto.getType() == 0) { // 不喜欢
            // 判断是否网络延迟
            if (cacheService.hGet(key, user.getId() + "") != null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST);
            }
            cacheService.hPut(key, user.getId() + "", JSON.toJSONString(dto));

        } else { // 取消不喜欢
            cacheService.hDelete(key, user.getId()+ "");
        }
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
