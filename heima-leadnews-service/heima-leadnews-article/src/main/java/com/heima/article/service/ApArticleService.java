package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.dtos.ArticleInfoDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.mess.ArticleVisitStreamMess;

/**
 * ClassName: ApArticleService
 * Package: com.heima.article.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/24 20:29
 * @Version 11
 */
public interface ApArticleService extends IService<ApArticle> {
    /**
     * 根据参数加载文章列表
     * @param loadType 1为加载更多 2为加载最新
     * @param dto
     * @return
     */
    ResponseResult load(Short loadType, ArticleHomeDto dto);

    ResponseResult saveArticle(ArticleDto dto);

    /**
     * 加载文章列表
     * @param loadType  1为加载更多 2为加载最新
     * @param dto
     * @param firstPage true 首页  false 非首页
     * @return
     */
    ResponseResult load2(Short loadType, ArticleHomeDto dto,boolean firstPage);

    /**
     * 加载文章行为
     * @param dto
     * @return
     */
    ResponseResult loadArticle(ArticleInfoDto dto);

    /**
     * 更新文章的分值  同时更新缓存中的热点文章数据
     * @param streamMess
     */
    void updateScore(ArticleVisitStreamMess streamMess);
}
