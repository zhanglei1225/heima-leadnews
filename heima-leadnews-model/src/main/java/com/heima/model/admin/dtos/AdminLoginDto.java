package com.heima.model.admin.dtos;

import lombok.Data;

/**
 * ClassName: AdminLoginDto
 * Package: com.heima.model.admin.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 16:31
 * @Version 11
 */
@Data
public class AdminLoginDto {
    /**
     * 用户名
     */
    private String name;
    /**
     * 密码
     */
    private String password;
}
