package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * ClassName: ApArticleMapper
 * Package: com.heima.article.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/24 19:58
 * @Version 11
 */
@Mapper
public interface ApArticleMapper extends BaseMapper<ApArticle> {


    /**
     * 加载文章列表
     * @param dto
     * @param type  1  加载更多   2记载最新
     * @return
     */
    List<ApArticle> loadArticleList(@Param("dto") ArticleHomeDto dto,@Param("type") Short type);

    /**
     * 加载最近五天文章
     * @param dayParam
     * @return
     */
    List<ApArticle> findArticleListByLast5Days(@Param("dayParam") Date dayParam);
}
