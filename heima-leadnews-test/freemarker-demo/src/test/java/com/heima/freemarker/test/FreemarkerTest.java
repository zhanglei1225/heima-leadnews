package com.heima.freemarker.test;

import com.heima.freemarker.FreemarkerDemoApplication;
import com.heima.freemarker.entity.Student;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * ClassName: FreemarkerTest
 * Package: com.heima.freemarker.test
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/25 15:03
 * @Version 11
 */
@SpringBootTest(classes = FreemarkerDemoApplication.class)
@RunWith(SpringRunner.class)
public class FreemarkerTest {

    @Autowired
    private Configuration configuration;
    @Test
    public void test1() throws IOException, TemplateException {
        Template template = configuration.getTemplate("02-list.ftl");
        /**
         * 合成方法
         * 两个参数
         * 第一个参数  模型数据
         * 第二个参数  输出流
         */
        template.process(getData(),new FileWriter("C:\\5kqgis\\FileTest\\leadNewsTest\\list.html"));
    }
    private Map getData(){

        HashMap<String, Object> hashMap = new HashMap<>();

        Student stu1 = new Student();
        stu1.setName("小强");
        stu1.setAge(18);
        stu1.setMoney(1000.86f);
        stu1.setBirthday(new Date());

        //小红对象模型数据
        Student stu2 = new Student();
        stu2.setName("小红");
        stu2.setMoney(200.1f);
        stu2.setAge(19);

        //将两个对象模型数据存放到List集合中
        List<Student> stus = new ArrayList<>();
        stus.add(stu1);
        stus.add(stu2);

        HashMap<String, Student> map = new HashMap<>();
        map.put("stu1",stu1);
        map.put("stu2",stu2);
        hashMap.put("stumap",map);

        //向model中存放List集合数据
        hashMap.put("stus",stus);
        //------------------------------------

        //创建Map数据
        HashMap<String,Student> stuMap = new HashMap<>();
        stuMap.put("stu1",stu1);
        stuMap.put("stu2",stu2);
        // 3.1 向model中存放Map数据
        hashMap.put("stuMap", stuMap);
        // 日期
        hashMap.put("today",new Date());
        // 长数值
        hashMap.put("point",123456789L);
        return hashMap;
    }
}
