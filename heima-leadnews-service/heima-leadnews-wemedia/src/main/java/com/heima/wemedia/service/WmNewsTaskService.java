package com.heima.wemedia.service;

import com.heima.model.wemedia.pojos.WmNews;

/**
 * ClassName: WmNewsTaskService
 * Package: com.heima.wemedia.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/2 20:30
 * @Version 11
 */
public interface WmNewsTaskService {
    /**
     * 发布延迟任务
     * @param wmNews
     */
    void saveTask(WmNews wmNews);

    /**
     * 定时拉取已经到期的任务
     */
    void pollTask();

}
