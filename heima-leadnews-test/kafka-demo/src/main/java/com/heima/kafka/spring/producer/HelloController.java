package com.heima.kafka.spring.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: HelloController
 * Package: com.heima.kafka.spring.producer
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/9 17:20
 * @Version 11
 */
@RestController
public class HelloController {

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    @GetMapping("/hello")
    public String hello(){
        for (int i = 0; i < 5; i++) {
            if (i%2==0) {
                kafkaTemplate.send("itheima-topic", "hello kafka");
            }else {
                kafkaTemplate.send("itheima-topic", "hello itcast");
            }
        }
        return "success";
    }
}
