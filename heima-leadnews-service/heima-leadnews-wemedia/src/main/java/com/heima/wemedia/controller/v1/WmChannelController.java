package com.heima.wemedia.controller.v1;

import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.wemedia.service.WmChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: WmChannelController
 * Package: com.heima.wemedia.controller.v1
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 16:04
 * @Version 11
 */
@RestController
@RequestMapping("/api/v1/channel")
public class WmChannelController {
    @Autowired
    private WmChannelService wmChannelService;

    /**
     * 查询所有频道
     * @return
     */
    @GetMapping("/channels")
    public ResponseResult finaAll(){
        return wmChannelService.findAll();
    }

    @GetMapping("/del/{id}")
    public ResponseResult delChannel(@PathVariable Integer id){
        return wmChannelService.delChannel(id);
    }

    @PostMapping("/save")
    public ResponseResult saveChannel(@RequestBody WmChannel wmChannel){
        return wmChannelService.saveChannel(wmChannel);
    }

    @PostMapping("/list")
    public ResponseResult listChannels(@RequestBody ChannelDto dto){
        return wmChannelService.listChannels(dto);
    }

    @PostMapping("/update")
    public ResponseResult updateChannels(@RequestBody WmChannel wmChannel){
        return wmChannelService.updateChannels(wmChannel);
    }
}
