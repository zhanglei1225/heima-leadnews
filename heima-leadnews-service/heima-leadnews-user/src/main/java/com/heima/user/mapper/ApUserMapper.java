package com.heima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.user.pojos.ApUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: ApUserMapper
 * Package: com.heima.user.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/21 18:15
 * @Version 11
 */
@Mapper
public interface ApUserMapper extends BaseMapper<ApUser> {
}
