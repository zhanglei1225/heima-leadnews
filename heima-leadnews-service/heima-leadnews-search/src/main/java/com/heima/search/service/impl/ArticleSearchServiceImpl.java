package com.heima.search.service.impl;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.search.service.ApUserSearchService;
import com.heima.search.service.ArticleSearchService;
import com.heima.utils.thread.AppThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * ClassName: ArticleSearchServiceImpl
 * Package: com.heima.search.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/5 14:50
 * @Version 11
 */
@Service
@Slf4j
public class ArticleSearchServiceImpl implements ArticleSearchService {

    @Autowired
    private RestHighLevelClient client;
    @Autowired
    private ApUserSearchService apUserSearchService;

    /**
     * ES文章分页搜索
     *
     * @param userSearchDto
     * @return
     */
    @Override
    public ResponseResult search(UserSearchDto userSearchDto) {
        //1.检查参数
        if (userSearchDto==null || StringUtils.isBlank(userSearchDto.getSearchWords())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        ApUser user = AppThreadLocalUtil.getUser();
        //异步调用 保存搜索记录
        if (user!=null && userSearchDto.getFromIndex()==0) {
            apUserSearchService.insert(userSearchDto.getSearchWords(),user.getId());
        }

        //2.设置查询条件
        SearchRequest searchRequest = new SearchRequest("app_info_article");
        //布尔查询 用于将多个条件进行组合
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //关键字的分词之后查询
        MultiMatchQueryBuilder multiMatchQuery = QueryBuilders.multiMatchQuery(userSearchDto.getSearchWords(), "title", "content");
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("publishTime").lt(userSearchDto.getMinBehotTime());
        // 将range查询和multiMatch查询通过boolQuery进行组合
        boolQueryBuilder.must(multiMatchQuery).filter(rangeQueryBuilder);

        // 设置查询条件
        searchRequest.source().query(boolQueryBuilder);
        // 甚至每页显示条数
        searchRequest.source().size(userSearchDto.getPageSize());
        // 指定排序
        searchRequest.source().sort("publishTime", SortOrder.DESC);
        // 指定高亮
        searchRequest.source().highlighter(new HighlightBuilder()
                .field("title").preTags("<em style ='color:red;font-size:36px'>") // 0.5rem
                .postTags("</em>").requireFieldMatch(false));


        ArrayList<Map> list =null;
        try {
            SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
            //3.结果封装返回
            list = new ArrayList<>();
            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                //处理高亮
                if (hit.getHighlightFields()!=null && hit.getHighlightFields().size()>0) {
                    Text[] titles = hit.getHighlightFields().get("title").getFragments();
                    String title = StringUtils.join(titles);
                    //高亮标题
                    map.put("h_title",title);
                }else {
                    //原始标题
                    map.put("h_title",map.get("title"));
                }
                list.add(map);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseResult.okResult(list);

    }
}
