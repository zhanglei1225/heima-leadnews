package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.springframework.web.multipart.MultipartFile;

/**
 * ClassName: WmMaterialService
 * Package: com.heima.wemedia.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 10:39
 * @Version 11
 */
public interface WmMaterialService extends IService<WmMaterial> {

    /**
     * 图片上传
     * @param multipartFile
     * @return
     */
    ResponseResult uploadPicture(MultipartFile multipartFile);

    /**
     * 素材列表查询
     * @param dto
     * @return
     */
    ResponseResult findList(WmMaterialDto dto);

    // 图片删除
    ResponseResult delPicture(Integer id);

    // 素材收藏或取消
    ResponseResult cancelOrCollect(Integer id, short isCollection);
}
