package com.heima.model.wemedia.dtos;

import lombok.Data;

/**
 * ClassName: WmNewsDto1
 * Package: com.heima.model.wemedia.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/1 12:59
 * @Version 11
 */
@Data
public class WmNewsDto1 {
    private Integer id;
    /**
     * 是否上架 0下架  1上架
     */
    private Short enable;
}
