package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsDto1;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;

/**
 * ClassName: WmNewsService
 * Package: com.heima.wemedia.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 18:50
 * @Version 11
 */
public interface WmNewsService extends IService<WmNews> {
    // 查询文章
    ResponseResult findAll(WmNewsPageReqDto wmNewsPageReqDto);

    // 文章发布
    ResponseResult submitNews(WmNewsDto dto);

    /**
     * 查看详情
     * @param id
     * @return
     */
    ResponseResult showDetails(Integer id);

    /**
     * 文章删除
     * @param id
     * @return
     */
    ResponseResult deleteNews(Integer id);

    /**
     * 文章上下架
     * @param wmNewsDto
     * @return
     */
    ResponseResult downOrUp(WmNewsDto1 wmNewsDto1);
}
