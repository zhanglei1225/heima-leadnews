package com.heima.tess4j;

import net.sourceforge.tess4j.Tesseract;

import java.io.File;

/**
 * ClassName: Application
 * Package: com.heima.tess4j
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/30 20:28
 * @Version 11
 */
public class Application {
    public static void main(String[] args) {
        try {
        // 获取本地图片
        File file = new File("C:\\4soft\\leiWei\\09-项目三黑马头条\\day04-自媒体文章审核\\资料\\image-1.png");
        //创建Tesseract对象
        Tesseract tesseract = new Tesseract();
        //设置字体库路径
        tesseract.setDatapath("C:\\5kqgis\\FileTest\\tessdata");
        //中文识别
        tesseract.setLanguage("chi_sim");
        //执行ocr识别
        String result = tesseract.doOCR(file);
        //替换回车和tal键  使结果为一行
            result=result.replaceAll("\\r/\\n","-").replaceAll(" ","");
            System.out.println("识别的结果为："+result);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
