package com.heima.freemarker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ClassName: FreemarkerDemoApplication
 * Package: com.heima.freemarker
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/25 10:30
 * @Version 11
 */
@SpringBootApplication
public class FreemarkerDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(FreemarkerDemoApplication.class,args);
    }
}
