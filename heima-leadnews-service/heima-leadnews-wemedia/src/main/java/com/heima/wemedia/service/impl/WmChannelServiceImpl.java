package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.service.WmChannelService;
import com.heima.wemedia.service.WmNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * ClassName: WmChannelServiceImpl
 * Package: com.heima.wemedia.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 16:54
 * @Version 11
 */
@Service
public class WmChannelServiceImpl extends ServiceImpl<WmChannelMapper, WmChannel> implements WmChannelService {

    @Autowired
    private WmChannelMapper wmChannelMapper;
    @Autowired
    private WmNewsService wmNewsService;

    @Override
    public ResponseResult findAll() {
        return ResponseResult.okResult(list());
    }

    /**
     * 保存频道
     *
     * @param wmChannel
     * @return
     */
    @Override
    public ResponseResult saveChannel(WmChannel wmChannel) {
        if (wmChannel==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        WmChannel one = getOne(Wrappers.<WmChannel>lambdaQuery().eq(WmChannel::getName, wmChannel.getName()));
        if (one!=null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST,"频道名称不能重复");
        }
        wmChannel.setCreatedTime(new Date());
        wmChannel.setIsDefault(true);
        save(wmChannel);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 频道名称模糊分页查询
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult listChannels(ChannelDto dto) {
        if (dto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        List<WmChannel> wmChannels = wmChannelMapper.listChannels(dto);
        return ResponseResult.okResult(wmChannels);
    }

    /**
     * 修改频道
     *
     * @param wmChannel
     * @return
     */
    @Override
    public ResponseResult updateChannels(WmChannel wmChannel) {
        if (wmChannel==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (wmChannel.getId()!=null) {
            List<WmNews> wmNewsList = wmNewsService.list(Wrappers.<WmNews>lambdaQuery().eq(WmNews::getChannelId, wmChannel.getId()));
            if (wmNewsList!=null && wmNewsList.size()>0) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE,"频道被引用则不能禁用");
            }
        }
        updateById(wmChannel);
        return ResponseResult.okResult(wmChannel);
    }

    /**
     * 删除频道
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult delChannel(Integer id) {
        if (id==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        WmChannel wmChannel = getById(id);
        if (wmChannel.getStatus()) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE,"只有禁用的频道才能删除");
        }
        removeById(id);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
