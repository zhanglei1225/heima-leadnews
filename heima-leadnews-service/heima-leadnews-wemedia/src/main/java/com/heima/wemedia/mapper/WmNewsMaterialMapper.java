package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * ClassName: WmNewsMaterialMapper
 * Package: com.heima.wemedia.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 19:47
 * @Version 11
 */
@Mapper
public interface WmNewsMaterialMapper extends BaseMapper<WmNewsMaterial> {
    void saveRelations(@Param("materialIds") List<Integer> materialIds,@Param("newsId")Integer newsId,
                       @Param("type")Short type);


}
