package com.heima.kafka.spring.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * ClassName: MyListener
 * Package: com.heima.kafka.spring.listener
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/9 17:16
 * @Version 11
 */
@Component
public class MyListener {

    @KafkaListener(topics = "itcast-topic")
    public void onMessage(ConsumerRecord<String, String> consumerRecord){
        System.out.println(consumerRecord.key()+"---->"+consumerRecord.value());
    }
}
