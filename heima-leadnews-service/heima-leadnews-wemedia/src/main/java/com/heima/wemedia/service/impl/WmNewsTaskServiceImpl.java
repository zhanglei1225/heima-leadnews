package com.heima.wemedia.service.impl;

import com.heima.apis.schedule.ITaskClient;
import com.heima.common.constants.ScheduleConstants;
import com.heima.common.enums.TaskTypeEnum;
import com.heima.model.schedule.dtos.Task;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.utils.common.ProtostuffUtil;
import com.heima.wemedia.service.WmNewsAutoScanService;
import com.heima.wemedia.service.WmNewsTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * ClassName: WmNewsTaskServiceImpl
 * Package: com.heima.wemedia.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/2 20:30
 * @Version 11
 */
@Service
public class WmNewsTaskServiceImpl implements WmNewsTaskService {
    @Autowired
    private ITaskClient iTaskClient;
    @Autowired
    private WmNewsAutoScanService wmNewsAutoScanService;
    /**
     * 发布延迟任务
     *
     * @param wmNews
     */
    @Override
    public void saveTask(WmNews wmNews) {
        // 发起feign远程调用，添加延迟任务
        Task task = new Task();
        task.setExecuteTime(wmNews.getPublishTime().getTime());  //设置过期时间
        task.setTaskType(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType());
        task.setPriority(TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
        WmNews wmNews1 = new WmNews();
        wmNews1.setId(wmNews.getId());
        task.setParameters(ProtostuffUtil.serialize(wmNews1));
        iTaskClient.addTsk(task);
    }

    /**
     * 定时拉取已经到期的任务
     */
    @Override
    @Scheduled(fixedDelay = 1000)
    public void pollTask() {
        Task task = iTaskClient.poll(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType(),TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
        if (task!=null) {
            byte[] parameters = task.getParameters();
            WmNews wmNews = ProtostuffUtil.deserialize(parameters, WmNews.class);
            wmNewsAutoScanService.autoScanWmNews(wmNews.getId());
        }
    }

}
