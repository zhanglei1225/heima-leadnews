package com.heima.model.behavior.dtos;

import lombok.Data;

/**
 * ClassName: likeBehaviorDto
 * Package: com.heima.model.behavior.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 9:59
 * @Version 11
 */
@Data
public class LikesBehaviorDto {

    /**
     * 文章id
     */
    private Long articleId;

    /**
     * 0 点赞   1 取消点赞
     */
    private Short operation;

    /**
     * 0文章  1动态   2评论
     */
    private Short type;
}
