package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.constants.BehaviorConstants;
import com.heima.common.redis.CacheService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.dtos.UserRelationDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserService;
import com.heima.utils.common.AppJwtUtil;
import com.heima.utils.thread.AppThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.HashMap;

/**
 * ClassName: ApUserServiceImpl
 * Package: com.heima.user.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/21 18:22
 * @Version 11
 */
@Service
@Transactional
@Slf4j
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {

    @Autowired
    private CacheService cacheService;

    /**
     * 登录功能
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult login(LoginDto dto) {
        // 1.正常登录
        String phone = dto.getPhone();
        String password = dto.getPassword();
        if (StringUtils.isNotBlank(phone) && StringUtils.isNotBlank(password)) {
            // 1.1 根据手机号获取用户信息
            ApUser apUser = getOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, phone));
            if (apUser == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
            } else {
                // 1.2 比对密码
                String salt = apUser.getSalt();
                if (!apUser.getPassword().equals(DigestUtils.md5DigestAsHex((password + salt).getBytes()))) {
                    return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
                }
                // 1.3 生成jwt 脱敏 返回token
                HashMap<String, Object> map = new HashMap<>();
                map.put("token", AppJwtUtil.getToken(apUser.getId().longValue()));
                apUser.setSalt("");
                apUser.setPassword("");
                map.put("user", apUser);
                return ResponseResult.okResult(map);
            }
        } else {
            // 2 游客登录
            HashMap<String, Object> map = new HashMap<>();
            map.put("token", 0L);
            return ResponseResult.okResult(map);
        }
    }

    /**
     * 关注与取消关注
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult follow(UserRelationDto dto) {
        // 参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        ApUser user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        String key = BehaviorConstants.USER_FOLLOW_BEHAVIOR + dto.getAuthorId();
        if (dto.getOperation() == 0) { // 关注
            // 判断是否网络延迟
            if (cacheService.sIsMember(key, user.getId() + "")) {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST);
            }
            cacheService.sAdd(key, user.getId() + "");

        } else { // 取消关注
            cacheService.sRemove(key, user.getId()+ "");
        }
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
