package com.heima.article.service;

import com.heima.model.article.pojos.ApArticle;

/**
 * ClassName: ArticleFreemarkerService
 * Package: com.heima.article.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/1 0:07
 * @Version 11
 */
public interface ArticleFreemarkerService {

    /**
     * 生成静态文件上传到minIO中
     * @param apArticle
     * @param content
     */
    void buildArticleToMinio(ApArticle apArticle,String content);
}
