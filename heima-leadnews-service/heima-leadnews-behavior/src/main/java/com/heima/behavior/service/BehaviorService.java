package com.heima.behavior.service;

import com.heima.model.behavior.dtos.CollectionBehaviorDto;
import com.heima.model.behavior.dtos.LikesBehaviorDto;
import com.heima.model.behavior.dtos.ReadBehaviorDto;
import com.heima.model.behavior.dtos.UnLikesBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

/**
 * ClassName: BehaviorService
 * Package: com.heima.behavior.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 10:16
 * @Version 11
 */
public interface BehaviorService {


    /**
     * 点赞与取消点赞操作
     * @param dto
     * @return
     */
    ResponseResult likeBehavior(LikesBehaviorDto dto);

    /**
     * 记录阅读行为
     * @param dto
     * @return
     */
    ResponseResult readBehavior(ReadBehaviorDto dto);

    /**
     * 收藏与取消收藏
     * @param dto
     * @return
     */
    ResponseResult collectioBehavior(CollectionBehaviorDto dto);

    /**
     * 不喜欢与取消不喜欢
     * @param dto
     * @return
     */
    ResponseResult unLikesBehavior(UnLikesBehaviorDto dto);
}
