package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.heima.article.service.ApArticleService;
import com.heima.common.constants.HotArticleConstants;
import com.heima.model.mess.ArticleVisitStreamMess;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * ClassName: HotArticleStreamListener
 * Package: com.heima.article.listener
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/10 0:32
 * @Version 11
 */
@Component
@Slf4j
public class HotArticleStreamListener {

    @Autowired
    private ApArticleService apArticleService;

    @KafkaListener(topics = HotArticleConstants.HOT_ARTICLE_INCR_HANDLE_TOPIC)
    public void doMessage(String message){

        if (StringUtils.isNotBlank(message)) {
            ArticleVisitStreamMess streamMess = JSON.parseObject(message, ArticleVisitStreamMess.class);
            apArticleService.updateScore(streamMess);
        }
    }
}
