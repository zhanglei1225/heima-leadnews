package com.heima.kafka.spring.stream;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.util.Arrays;

/**
 * ClassName: KafkaStreamHelloListener
 * Package: com.heima.kafka.spring.stream
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/9 17:33
 * @Version 11
 */
@Configuration
@Slf4j
public class KafkaStreamHelloListener {

    @Bean
    public KStream<String,String> kStream(StreamsBuilder streamsBuilder){
        // 1.指定要监听哪个上游主题 并获取到对应的流
        KStream<String,String> stream = streamsBuilder.stream("itheima-topic");
        // 2.对流进行统计
        stream.flatMapValues(new ValueMapper<String, Iterable<?>>() {
            @Override
            public Iterable<?> apply(String value) {
                return Arrays.asList(value.split(" "));
            }
        })
                .groupBy(new KeyValueMapper<String, Object, Object>() {
                    @Override
                    public Object apply(String key, Object value) {
                        return value;
                    }
                })
                .windowedBy(TimeWindows.of(Duration.ofSeconds(5)))
                .count()
                .toStream()
                .map(new KeyValueMapper<Windowed<Object>, Long, KeyValue<String,String>>() {
            @Override
            public KeyValue<String,String> apply(Windowed<Object> key, Long value) {
                return new KeyValue<String,String>(key.key().toString(),value.toString());
            }
        }).to("itcast-topic");
        return stream;
    }
}
