package com.heima.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.pojos.AdUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: AdUserMapper
 * Package: com.heima.admin.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 16:39
 * @Version 11
 */
@Mapper
public interface AdUserMapper extends BaseMapper<AdUser> {
}
