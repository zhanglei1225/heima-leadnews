package com.heima.model.user.dtos;

import lombok.Data;

/**
 * ClassName: UserRelationDto
 * Package: com.heima.model.user.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 13:00
 * @Version 11
 */
@Data
public class UserRelationDto  {
    /**
     * 文章id
     */
    private Long articleId;

    /**
     * 0  关注   1  取消
     */
    private Short operation;

    /**
     * 作者id
     */
    private Integer authorId;
}
