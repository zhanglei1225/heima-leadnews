package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmNews;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: WmNewsMapper
 * Package: com.heima.wemedia.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 18:53
 * @Version 11
 */
@Mapper
public interface WmNewsMapper extends BaseMapper<WmNews> {
}
