package com.heima.app.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * ClassName: AppGatewayApplication
 * Package: com.heima.app.gateway
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/24 10:54
 * @Version 11
 */
@SpringBootApplication
@EnableDiscoveryClient    //开启注册中心
public class AppGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppGatewayApplication.class,args);
    }
}
