package com.heima.wemedia.service;

/**
 * ClassName: WmNewsAutoScanService
 * Package: com.heima.wemedia.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/29 20:01
 * @Version 11
 */
public interface WmNewsAutoScanService {

    /**
     * 自媒体文章审核
     * @param id  自媒体文章id
     */
    void autoScanWmNews(Integer id);
}
