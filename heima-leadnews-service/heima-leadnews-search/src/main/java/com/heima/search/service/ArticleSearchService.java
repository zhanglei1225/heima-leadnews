package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

/**
 * ClassName: ArticleSearchService
 * Package: com.heima.search.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/5 14:23
 * @Version 11
 */
public interface ArticleSearchService {
    /**
     * ES文章分页搜索
     * @param userSearchDto
     * @return
     */
    ResponseResult search(UserSearchDto userSearchDto);
}
