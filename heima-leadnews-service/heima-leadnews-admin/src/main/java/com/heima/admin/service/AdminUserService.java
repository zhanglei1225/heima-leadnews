package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.AdminLoginDto;
import com.heima.model.admin.pojos.AdUser;
import com.heima.model.common.dtos.ResponseResult;

/**
 * ClassName: AdminUserService
 * Package: com.heima.admin.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 16:28
 * @Version 11
 */
public interface AdminUserService extends IService<AdUser> {


    ResponseResult login(AdminLoginDto dto);
}
