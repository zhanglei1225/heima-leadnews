package com.heima.model.behavior.dtos;

import lombok.Data;

/**
 * ClassName: ReadBehaviorDto
 * Package: com.heima.model.behavior.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 11:07
 * @Version 11
 */
@Data
public class ReadBehaviorDto {

    /**
     * 文章id
     */
    private Long articleId;

    /**
     * 阅读次数
     */
    private Integer count;
}
