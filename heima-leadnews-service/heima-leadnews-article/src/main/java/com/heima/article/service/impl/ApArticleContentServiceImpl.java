package com.heima.article.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleContent;
import org.springframework.stereotype.Service;

/**
 * ClassName: ApArticleContentServiceImpl
 * Package: com.heima.article.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/26 15:20
 * @Version 11
 */
@Service
public class ApArticleContentServiceImpl extends ServiceImpl<ApArticleContentMapper, ApArticleContent> {
}
