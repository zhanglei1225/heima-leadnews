package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.pojos.ApArticleContent;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: ApArticleContentMapper
 * Package: com.heima.article.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/25 20:15
 * @Version 11
 */
@Mapper
public interface ApArticleContentMapper extends BaseMapper<ApArticleContent> {
}
