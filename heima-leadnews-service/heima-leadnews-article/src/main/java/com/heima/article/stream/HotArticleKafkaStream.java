package com.heima.article.stream;

import com.alibaba.fastjson.JSON;
import com.heima.common.constants.HotArticleConstants;
import com.heima.model.mess.ArticleVisitStreamMess;
import com.heima.model.mess.UpdateArticleMess;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

/**
 * ClassName: HotArticleKafkaStream
 * Package: com.heima.article.stream
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/9 20:00
 * @Version 11
 */
@Configuration
@Slf4j
public class HotArticleKafkaStream {
    @Bean
    public KStream<String, String> process(StreamsBuilder streamsBuilder) {
        KStream<String, String> stream = streamsBuilder.stream(HotArticleConstants.HOT_ARTICLE_SCORE_TOPIC);
        stream.map(new KeyValueMapper<String, String, KeyValue<String, String>>() {
            @Override
            public KeyValue<String, String> apply(String key, String value) {
                UpdateArticleMess updateArticleMess = JSON.parseObject(value, UpdateArticleMess.class);
                String value1 = updateArticleMess.getType() + ":" + updateArticleMess.getAdd();
                return new KeyValue<String, String>(updateArticleMess.getArticleId() + "", value1);
            }
        }).groupByKey().windowedBy(TimeWindows.of(Duration.ofSeconds(5)))
                // 自行的完成聚合的计算
                .aggregate(new Initializer<String>() {  // 初始方法，返回值是消息的value
                    @Override
                    public String apply() {
                        return "COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:0";
                    }
                }, new Aggregator<String, String, String>() {  // 真正的聚合操作，返回值是消息的value
                    @Override
                    public String apply(String key, String value, String init) {
                        if (StringUtils.isBlank(value)) {
                            return init;
                        }
                        int likes = 0, views = 0, comment = 0, collection = 0;
                        // 获得初始值，也是时间窗口内计算之后的值
                        String[] split1 = init.split(","); // split[1]=["likes:0","views:1"]
                        for (String s : split1) {
                            String[] split = s.split(":");
                            switch (split[0]){
                                case "COLLECTION":
                                    collection = Integer.parseInt(split[1]);
                                    break;
                                case "COMMENT":
                                    comment = Integer.parseInt(split[1]);
                                    break;
                                case "LIKES":
                                    likes = Integer.parseInt(split[1]);
                                    break;
                                case "VIEWS":
                                    views = Integer.parseInt(split[1]);
                                    break;
                            }
                        }
                        // 累加操作
                        String[] split = value.split(":"); // split[0]="views"  split[1]=1
                        switch (split[0]) {
                            case "COLLECTION":
                                collection += Integer.parseInt(split[1]);
                                break;
                            case "COMMENT":
                                comment += Integer.parseInt(split[1]);
                                break;
                            case "LIKES":
                                likes += Integer.parseInt(split[1]);
                                break;
                            case "VIEWS":
                                views += Integer.parseInt(split[1]);
                                break;
                        }
                        return String.format("COLLECTION:%d,COMMENT:%d,LIKES:%d,VIEWS:%d",collection,comment,likes,views);
                    }
                }, Named.as("hotArticle_stream"))
                .toStream()
                .map(new KeyValueMapper<Windowed<String>, String, KeyValue<String, String>>() {
            @Override
            public KeyValue<String, String> apply(Windowed<String> key, String value) {
                return new KeyValue<String, String>(key.key(),formatData(key.key(),value));
            }
        })
                .to(HotArticleConstants.HOT_ARTICLE_INCR_HANDLE_TOPIC);
        return stream;
    }

    /**
     * 格式化消息的value数据
     * @param key
     * @param value
     * @return
     */
    private String formatData(String key, String value) {
        ArticleVisitStreamMess mess = new ArticleVisitStreamMess();
        mess.setArticleId(Long.valueOf(key));
        //COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:0
        String[] split = value.split(",");
        for (String s : split) {
            String[] split1 = s.split(":");
            switch (split1[0]){
                case "COLLECTION":
                    mess.setCollect(Integer.parseInt(split1[1]));
                    break;
                case "COMMENT":
                    mess.setComment(Integer.parseInt(split1[1]));
                    break;
                case "LIKES":
                    mess.setLike(Integer.parseInt(split1[1]));
                    break;
                case "VIEWS":
                    mess.setView(Integer.parseInt(split1[1]));
                    break;
            }
        }
        log.info("聚合消息处理之后的结果为:{}",JSON.toJSONString(mess));
        return JSON.toJSONString(mess);
    }
}
