package com.heima.model.article.dtos;

import lombok.Data;

/**
 * ClassName: ArticleInfoDto
 * Package: com.heima.model.article.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 14:30
 * @Version 11
 */
@Data
public class ArticleInfoDto {

    /**
     * 文章id
     */
    private Long articleId;

    /**
     * 作者id
     */
    private Integer authorId;
}
