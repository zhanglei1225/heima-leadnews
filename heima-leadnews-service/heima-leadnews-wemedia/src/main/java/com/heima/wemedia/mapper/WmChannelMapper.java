package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.wemedia.pojos.WmChannel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * ClassName: WmChannelMapper
 * Package: com.heima.wemedia.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 16:54
 * @Version 11
 */
@Mapper
public interface WmChannelMapper extends BaseMapper<WmChannel> {
    List<WmChannel> listChannels(@Param("dto") ChannelDto dto);
}
