package com.heima.kafka.sample;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

/**
 * ClassName: KafkaStreamQuickStart
 * Package: com.heima.kafka.sample
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/9 11:26
 * @Version 11
 */
// 流式处理
public class KafkaStreamQuickStart {
    public static void main(String[] args) {
        //kafka的配置信息
        Properties properties = new Properties();
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.200.130:9092");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-quickStart");

        //stream 构建器
        StreamsBuilder streamsBuilder = new StreamsBuilder();
        //流式计算
        streamProcessor(streamsBuilder);
        Topology topology = streamsBuilder.build();
        KafkaStreams kafkaStreams = new KafkaStreams(topology, properties); // 创建流处理对象
        kafkaStreams.start(); //开始工作
    }

    /**
     * 流式计算
     * 消息的内容：hello kafka  hello itcast
     *
     * @param streamsBuilder
     */
    private static void streamProcessor(StreamsBuilder streamsBuilder) {
        //创建kstream对象，同时指定从那个topic中接收消息
        KStream<String, String> kStream = streamsBuilder.stream("itheima-topic");
        // 处理消息的value
        kStream.flatMapValues(new ValueMapper<String, Iterable<String>>() {
            @Override
            public Iterable<String> apply(String s) {
                return Arrays.asList(s.split(" "));
            }
        })          // 根据key来分组
                .groupBy(new KeyValueMapper<String, String, Object>() {
                    @Override
                    public Object apply(String key, String value) {
                        return value;
                    }
                })
                //时间窗口
                .windowedBy(TimeWindows.of(Duration.ofSeconds(5)))
                .count() // 统计每组元素的个数
                .toStream()
                .map(new KeyValueMapper<Windowed<Object>, Long, KeyValue<String, String>>() {
                    @Override
                    public KeyValue<String, String> apply(Windowed<Object> key, Long value) {
                        return new KeyValue<>(key.key().toString(), String.valueOf(value));
                    }
                }).to("itcast-topic");

    }
}
