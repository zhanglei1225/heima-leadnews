package com.heima.xxljob.job;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * ClassName: HelloJob
 * Package: com.heima.xxljob.job
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/6 17:24
 * @Version 11
 */
@Component
@Slf4j
public class HelloJob {

    @XxlJob("demoJobHandler")
    public void helloJob() {
        log.info("简单任务执行了");
    }

    @XxlJob("shardingJobHandler")
    public void shardingJobHandler() {
        //分片的参数
        // 获取当前实例的索引
        int shardIndex = XxlJobHelper.getShardIndex();
        // 获取所有实例个数
        int shardTotal = XxlJobHelper.getShardTotal();
//        log.info("当前实例索引，{}，总实例个数，{}",shardIndex,shardTotal);
//        log.info("select * from ap_user where id %2 =="+shardIndex);
        //业务逻辑
        List<Integer> list = getList();
        for (Integer integer : list) {
            if (integer % shardTotal ==shardIndex) {
                System.out.println("当前第"+shardIndex+"分片执行了，任务项为:"+integer);
            }
        }
    }

    private List<Integer> getList() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            arrayList.add(i);
        }
        return arrayList;
    }
}
