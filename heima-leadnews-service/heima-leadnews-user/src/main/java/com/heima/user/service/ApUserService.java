package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.dtos.UserRelationDto;
import com.heima.model.user.pojos.ApUser;

/**
 * ClassName: ApUserService
 * Package: com.heima.user.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/21 18:21
 * @Version 11
 */
public interface ApUserService extends IService<ApUser> {
    /**
     * 登录功能
     * @param dto
     * @return
     */
    ResponseResult login(LoginDto dto);

    /**
     * 关注与取消关注
     * @param dto
     * @return
     */
    ResponseResult follow(UserRelationDto dto);
}
