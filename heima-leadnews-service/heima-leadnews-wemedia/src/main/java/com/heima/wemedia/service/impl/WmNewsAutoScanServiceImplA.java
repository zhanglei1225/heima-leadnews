package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.heima.apis.article.IArticleClient;
import com.heima.common.aliyun.GreenImageScan;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.wemedia.service.WmChannelService;
import com.heima.wemedia.service.WmNewsAutoScanService;
import com.heima.wemedia.service.WmNewsService;
import com.heima.wemedia.service.WmUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * ClassName: WmNewsAutoScanServiceImplA
 * Package: com.heima.wemedia.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/1 20:16
 * @Version 11
 */
@Service
@Slf4j
@Transactional
public class WmNewsAutoScanServiceImplA  {
    @Autowired
    private WmNewsService wmNewsService;
    @Autowired
    private GreenTextScan greenTextScan;
    @Autowired
    private GreenImageScan greenImageScan;
    @Autowired
    private FileStorageService fileStorageService;
    @Resource
    private IArticleClient iArticleClient;
    @Autowired
    private WmChannelService wmChannelService;
    @Autowired
    private WmUserService wmUserService;

    /**
     * 自媒体文章审核
     *
     * @param id 自媒体文章id
     */
    @Async
    public void autoScanWmNews(Integer id) {
        // 查询文章
        WmNews wmNews = wmNewsService.getById(id);
        if (wmNews == null) {
            throw new RuntimeException("文章不存在");
        }
        // 获取纯文本内容和图片
        Map<String, Object> map = handleTextAndImage(wmNews);
        // 审核文本内容  失败2 人工3
        boolean is_text = autoScanText(wmNews, map);
        if (!is_text) {
            return;
        }
        // 审核图片  失败2 人工3
        boolean is_image = autoScanImage(wmNews, (List<String>)map.get("images"));
        if (!is_image) {
            return;
        }
        // 审核成功  保存app端文章相关信息
        ResponseResult responseResult = saveAppArticle(wmNews);
        if (!responseResult.getCode().equals(200)) {
            throw new RuntimeException("文章审核，保存app端相关文章数据失败");
        }
        // 已发布 9  回填 article_id
        wmNews.setArticleId((long)responseResult.getData());
        updateWmNews(wmNews,"审核成功",(short) 9);
    }

    /**
     * 保存app端文章相关信息
     * @param wmNews
     * @return
     */
    private ResponseResult saveAppArticle(WmNews wmNews) {
        ArticleDto articleDto = new ArticleDto();
        BeanUtils.copyProperties(wmNews,articleDto);
        // 布局 频道名称  作者id和名称 文章id 创建时间
        articleDto.setLayout(wmNews.getType());
        WmChannel wmChannel = wmChannelService.getById(wmNews.getChannelId());
        articleDto.setChannelName(wmChannel.getName());
        articleDto.setAuthorId((long) wmNews.getUserId());
        WmUser wmUser = wmUserService.getById(wmNews.getUserId());
        if (wmUser!=null) {
            articleDto.setAuthorName(wmUser.getName());
        }
        if (wmNews.getArticleId()!=null) {
            articleDto.setId(wmNews.getArticleId());
        }
        articleDto.setCreatedTime(new Date());
        ResponseResult responseResult = iArticleClient.saveArticle(articleDto);
        return responseResult;
    }

    /**
     * 审核图片
     * @param wmNews
     * @param images
     * @return
     */
    private boolean autoScanImage(WmNews wmNews, List<String> images) {
        boolean flag = true;
        if (images ==null || images.size()==0) {
            return flag;
        }
        // 去重
        images=images.stream().distinct().collect(Collectors.toList());
        // 从minio下载图片
        List<byte[]> imageList = new ArrayList<>();
        for (String image : images) {
            byte[] bytes = fileStorageService.downLoadFile(image);
            imageList.add(bytes);
        }
        try {
            Map map1 = greenImageScan.imageScan(imageList);
            if (map1 != null) {
                if (map1.get("suggestion").equals("block")) {
                    updateWmNews(wmNews, "图片审核不通过", (short) 2);
                    flag =false;
                }
                if (map1.get("suggestion").equals("review")) {
                    updateWmNews(wmNews, "图片需要人工审核", (short) 3);
                    flag =false;
                }
            }
        } catch (Exception exception) {
            flag = false;
            exception.printStackTrace();
        }
        return flag;
    }

    /**
     * 审核文本内容
     *
     * @param wmNews
     * @param map
     * @return
     */
    private boolean autoScanText(WmNews wmNews, Map<String, Object> map) {
        boolean flag = true;
        try {
            if ((map.get("content")+wmNews.getTitle()).length()==0){
                return flag;
            }
            Map map1 = greenTextScan.greeTextScan( map.get("content")+wmNews.getTitle());
            if (map1 != null) {
                if (map1.get("suggestion").equals("block")) {
                    updateWmNews(wmNews, "当前文章中存在违规内容", (short) 2);
                    flag =false;
                }
                if (map1.get("suggestion").equals("review")) {
                    updateWmNews(wmNews, "当前文章中存在不确定内容", (short) 3);
                    flag =false;
                }
            }
        } catch (Exception exception) {
            flag = false;
            exception.printStackTrace();
        }
        return flag;
    }

    /**
     * 更新文章状态
     *
     * @param wmNews
     */
    private void updateWmNews(WmNews wmNews, String reason, short status) {
        wmNews.setStatus(status);
        wmNews.setReason(reason);
        wmNewsService.updateById(wmNews);
    }

    /**
     * 获取纯文本内容和图片
     *
     * @param wmNews
     * @return
     */
    private Map<String, Object> handleTextAndImage(WmNews wmNews) {
        StringBuilder text = new StringBuilder();
        ArrayList<String> images = new ArrayList<>();
            List<Map> maps = JSONArray.parseArray(wmNews.getContent(), Map.class);
            for (Map map : maps) {
                if (map.get("type").equals("text")) {
                    text.append(map.get("value"));
                }
            }
        if (wmNews.getImages() != null) {
            String[] split = wmNews.getImages().split(",");
            Collections.addAll(images, split);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("content", text.toString());
        map.put("images", images);
        return map;
    }
}
