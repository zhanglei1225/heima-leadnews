package com.heima.common.constants;

/**
 * ClassName: BehaviorConstants
 * Package: com.heima.common.constants
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 10:27
 * @Version 11
 */
public class BehaviorConstants {

    public static final String LIKE_BEHAVIOR= "like_behavior_";
    public static final String UNLIKES_BEHAVIOR= "unlikes_behavior_";
    public static final String READ_BEHAVIOR= "read_behavior_";
    public static final String COLLECTION_BEHAVIOR= "collection_behavior_";
    public static final String USER_FOLLOW_BEHAVIOR= "user_follow_behavior_";
}
