package com.heima.apis.wemedia;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * ClassName: IWemediaClient
 * Package: com.heima.apis.wemedia
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/7 11:15
 * @Version 11
 */
@FeignClient("leadnews-wemedia")
public interface IWemediaClient {

    @GetMapping("/api/v1/channel/list")
    ResponseResult<List<WmChannel>> getChannels();
}
