package com.heima.apis.schedule;

import com.heima.model.schedule.dtos.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * ClassName: ITaskClient
 * Package: com.heima.apis.schedule
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/2 20:15
 * @Version 11
 */
@FeignClient("leadnews-schedule")
public interface ITaskClient {


    @PostMapping("api/v1/task/addTask")
    void addTsk(@RequestBody Task task);

    @GetMapping("api/v1/task/poll/{taskType}/{taskPriority}")
    Task poll(@PathVariable("taskType") int taskType,@PathVariable("taskPriority") int priority);


}
