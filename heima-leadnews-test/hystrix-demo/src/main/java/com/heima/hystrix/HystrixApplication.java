package com.heima.hystrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

/**
 * ClassName: HystrixApplication
 * Package: com.heima.hystrix
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/30 17:01
 * @Version 11
 */
@SpringBootApplication
@EnableCircuitBreaker
public class HystrixApplication {
    public static void main(String[] args) {
        SpringApplication.run(HystrixApplication.class, args);
    }
}
