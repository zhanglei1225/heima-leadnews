package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.search.pojos.ApUserSearch;

/**
 * ClassName: ApUserSearchService
 * Package: com.heima.search.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/5 19:50
 * @Version 11
 */
public interface ApUserSearchService {

    /**
     * 保存用户搜索历史记录
     * @param keyword
     * @param userId
     */
    void insert(String keyword,Integer userId);

    /**
     * 查询搜索历史
     * @return
     */
    ResponseResult findUserSearch();

    /**
     * 删除历史记录
     * @param apUserSearch
     * @return
     */
    ResponseResult delUserSearch(ApUserSearch apUserSearch);
}
