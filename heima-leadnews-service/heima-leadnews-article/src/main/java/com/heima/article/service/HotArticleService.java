package com.heima.article.service;

/**
 * ClassName: HotArticleService
 * Package: com.heima.article.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/7 11:30
 * @Version 11
 */
public interface HotArticleService {
    /**
     * 计算热点文章
     */
    void computeHotArticle();
}
