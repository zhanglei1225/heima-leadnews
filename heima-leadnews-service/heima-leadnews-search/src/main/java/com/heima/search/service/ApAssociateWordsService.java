package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

/**
 * ClassName: ApAssociateWordsService
 * Package: com.heima.search.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/6 15:55
 * @Version 11
 */
public interface ApAssociateWordsService {

    /**
     * 联想词
     * @param dto
     * @return
     */
    ResponseResult findAssociate(UserSearchDto dto);
}
