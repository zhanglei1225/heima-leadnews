package com.heima.user.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.dtos.UserRelationDto;
import com.heima.user.service.ApUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: ApUserController
 * Package: com.heima.user.controller.v1
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 12:58
 * @Version 11
 */
@RestController
@RequestMapping("/api/v1/user")
public class ApUserController {
    @Autowired
    private ApUserService apUserService;

    @PostMapping("user_follow")
    @ApiOperation(value = "用户关注")
    public ResponseResult follow(@RequestBody UserRelationDto dto){
        return apUserService.follow(dto);
    }
}
