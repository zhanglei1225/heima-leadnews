package com.heima.xxljob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ClassName: XxlJobApplication
 * Package: com.heima.xxljob
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/6 17:33
 * @Version 11
 */
@SpringBootApplication
public class XxlJobApplication {
    public static void main(String[] args) {
        SpringApplication.run(XxlJobApplication.class,args);
    }
}
