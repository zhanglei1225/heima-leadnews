package com.heima.model.behavior.dtos;

import lombok.Data;

import java.util.Date;

/**
 * ClassName: CollectionBehaviorDto
 * Package: com.heima.model.behavior.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 11:32
 * @Version 11
 */
@Data
public class CollectionBehaviorDto {

    /**
     * 文章id
     */
    private Long entryId;

    /**
     * 0收藏    1取消收藏
     */
    private Short operation;

    /**
     * 0文章  1动态   2评论
     */
    private Short type;

    /**
     * 发布时间
     */
    private Date publishedTime;
}
