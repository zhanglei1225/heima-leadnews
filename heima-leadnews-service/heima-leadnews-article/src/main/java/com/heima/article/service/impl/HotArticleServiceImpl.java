package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.apis.wemedia.IWemediaClient;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.HotArticleService;
import com.heima.common.constants.ArticleConstants;
import com.heima.common.redis.CacheService;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.vos.HotArticleVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ClassName: HotArticleServiceImpl
 * Package: com.heima.article.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/7 11:31
 * @Version 11
 */
@Service
@Slf4j
@Transactional
public class HotArticleServiceImpl implements HotArticleService {

    @Autowired
    private ApArticleMapper apArticleMapper;
    @Autowired
    private IWemediaClient wemediaClient;
    @Autowired
    private CacheService cacheService;

    /**
     * 计算热点文章
     */
    @Override
    public void computeHotArticle() {
        // 1.查询前五天文章数据
        Date date = DateTime.now().minusDays(5).toDate();
        List<ApArticle> articleList = apArticleMapper.findArticleListByLast5Days(date);
        // 2.计算文章分值
        List<HotArticleVo> hotArticleVoList = computeHotArticle(articleList);
        // 3.为每个频道缓存30条分值较高的文章
        cacheTagToRedis(hotArticleVoList);
    }

    /**
     * 为每个频道缓存30条分值较高的文章
     * @param hotArticleVoList
     */
    private void cacheTagToRedis(List<HotArticleVo> hotArticleVoList) {
        //每个频道缓存30条分值较高的文章
        ResponseResult<List<WmChannel>> responseResult = wemediaClient.getChannels();
        if (responseResult.getCode().equals(200)) {
            // 获取所有频道
//            String channelJson = JSON.toJSONString(responseResult.getData());
//            List<WmChannel> wmChannels = JSON.parseArray(channelJson, WmChannel.class);
            List<WmChannel> wmChannels = responseResult.getData();
            //检索出每个频道的文章
            if (wmChannels!=null && wmChannels.size()>0) {
                for (WmChannel wmChannel : wmChannels) {
                    List<HotArticleVo> hotArticleVos = hotArticleVoList.stream().filter(x -> wmChannel.getId()
                            .equals(x.getChannelId())).collect(Collectors.toList());
                    // 给文章进行排序，将分值较高的三十条文章存入redis  key= 频道id  value= 30篇文章
                    String key =ArticleConstants.HOT_ARTICLE_FIRST_PAGE +wmChannel.getId();
                    sortAndCache(hotArticleVos,key);
                }
            }
        }
        //设置推荐数据
        //给文章进行排序，取30条分值较高的文章存入redis  key：频道id   value：30条分值较高的文章
        String key =ArticleConstants.HOT_ARTICLE_FIRST_PAGE+ArticleConstants.DEFAULT_TAG;
        sortAndCache(hotArticleVoList,key);
    }

    /**
     * 排序并且缓存数据
     * @param hotArticleVos
     * @param key
     */
    private void sortAndCache(List<HotArticleVo> hotArticleVos, String key) {
        hotArticleVos = hotArticleVos.stream().sorted((o1, o2) -> o2.getScore()- o1.getScore()).collect(Collectors.toList());
        if (hotArticleVos.size()>30) {
            hotArticleVos = hotArticleVos.subList(0,30);
        }
        cacheService.set(key, JSON.toJSONString(hotArticleVos));
    }

    /**
     * 计算文章分值
     * @param articleList
     * @return
     */
    private List<HotArticleVo> computeHotArticle(List<ApArticle> articleList) {
        ArrayList<HotArticleVo> hotArticleVos = new ArrayList<>();
        if (articleList!=null && articleList.size()>0) {
            for (ApArticle apArticle : articleList) {
                HotArticleVo hotArticleVo = new HotArticleVo();
                BeanUtils.copyProperties(apArticle,hotArticleVo);
                Integer score = conputeScore(apArticle);
                hotArticleVo.setScore(score);
                hotArticleVos.add(hotArticleVo);
            }
        }
        return hotArticleVos;
    }

    /**
     * 计算文章具体分值
     * @param apArticle
     * @return
     */
    private Integer conputeScore(ApArticle apArticle) {
        Integer score =0;
        if (apArticle.getViews()!=null) {
           score+= apArticle.getViews();
        }
        if (apArticle.getLikes()!=null) {
            score+=apArticle.getLikes() * ArticleConstants.HOT_ARTICLE_LIKE_WEIGHT;
        }
        if (apArticle.getComment()!=null) {
            score+= apArticle.getComment()*ArticleConstants.HOT_ARTICLE_COMMENT_WEIGHT;
        }
        if (apArticle.getCollection()!=null) {
            score += apArticle.getCollection() *ArticleConstants.HOT_ARTICLE_COLLECTION_WEIGHT;
        }
        return score;
    }
}
