package com.heima.model.behavior.dtos;

import lombok.Data;

/**
 * ClassName: UnLikesBehaviorDto
 * Package: com.heima.model.behavior.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/8 11:39
 * @Version 11
 */
@Data
public class UnLikesBehaviorDto {

    /**
     * 文章id
     */
    private Long articleId;

    /**
     * 0 不喜欢      1 取消不喜欢
     */
    private Short type;
}
