package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.apis.article.IArticleClient;
import com.heima.common.constants.WemediaConstants;
import com.heima.common.constants.WmNewsMessageConstants;
import com.heima.common.exception.CustomException;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsDto1;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.service.WmNewsAutoScanService;
import com.heima.wemedia.service.WmNewsService;
import com.heima.wemedia.service.WmNewsTaskService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * ClassName: WmNewsServiceImpl
 * Package: com.heima.wemedia.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 18:52
 * @Version 11
 */
@Service
@Transactional
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews> implements WmNewsService {

    @Autowired
    private WmNewsMaterialMapper wmNewsMaterialMapper;
    @Autowired
    private WmMaterialMapper wmMaterialMapper;
    @Resource
    private WmNewsAutoScanService wmNewsAutoScanService;
    @Resource
    private IArticleClient iArticleClient;
    @Autowired
    private WmNewsTaskService wmNewsTaskService;
    @Autowired
    private KafkaTemplate kafkaTemplate;

    /**
     * 查询自媒体文章
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findAll(WmNewsPageReqDto dto) {

        // 参数检查
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        // 分页参数检查
        dto.checkParam();
        // 用户校验
        if (WmThreadLocalUtil.getUser() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        // 分页查询
        IPage page = new Page();
        LambdaQueryWrapper<WmNews> wrapper = new LambdaQueryWrapper<>();
        // 频道
        if (dto.getChannelId() != null) {
            wrapper.eq(WmNews::getChannelId, dto.getChannelId());
        }
        // 状态
        if (dto.getStatus() != null) {
            wrapper.eq(WmNews::getStatus, dto.getStatus());
        }
        // 发布时间范围查询
        if (dto.getBeginPubDate() != null && dto.getEndPubDate() != null) {
            wrapper.between(WmNews::getPublishTime, dto.getBeginPubDate(), dto.getEndPubDate());
        }
        // 关键字模糊查询
        if (StringUtils.isNotBlank(dto.getKeyword())) {
            wrapper.like(WmNews::getTitle, dto.getKeyword());
        }
        // 根据用户查询
        wrapper.eq(WmNews::getUserId, WmThreadLocalUtil.getUser().getId());
        // 发布时间倒序排序
        wrapper.orderByDesc(WmNews::getPublishTime);
        page = page(page, wrapper);
        // 返回结果
        PageResponseResult result = new PageResponseResult(dto.getPage(), dto.getSize(), (int) page.getTotal());
        result.setData(page.getRecords());
        return result;
    }

    /**
     * 文章发布
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult submitNews(WmNewsDto dto) {
        // 参数校验
        if (dto == null || StringUtils.isBlank(dto.getContent())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        // 用户校验
        if (WmThreadLocalUtil.getUser() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        // 保存或修改文章
        WmNews wmNews = new WmNews();
        // 属性拷贝，必须放在设置封面类型之前，不然拷贝之后类型覆盖，type=-1 超出范围
        BeanUtils.copyProperties(dto, wmNews);
        // 如果封面类型为自动
        if (dto.getType().equals(WemediaConstants.WM_NEWS_TYPE_AUTO)) {
            wmNews.setType(null);
        }
        // 封面图片转换 list-->string
        if (dto.getImages() != null && dto.getImages().size() > 0) {
            wmNews.setImages(StringUtils.join(dto.getImages(), ","));
        }
        saveOrUpdateInfo(dto, wmNews);
        // 判断是草稿还是文章发布
        if (dto.getStatus().equals(WmNews.Status.NORMAL.getCode())) {
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }
        // 提取文章内容图片路径
        List<String> images = extractUrlInfo(dto);
        // 保存文章内容图片与素材的关系
        saveRelationInfo(wmNews, images, WemediaConstants.WM_CONTENT_REFERENCE);
        // 保存文章封面图片与素材的关系
        saveRelationInfoForCover(dto, wmNews, images);

        // 将保存的文章封装成一个task对象，发给leadnews-schedule微服务
        wmNewsTaskService.saveTask(wmNews);
        //审核文章
//        wmNewsAutoScanService.autoScanWmNews(wmNews.getId());
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 查看详情
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult showDetails(Integer id) {
        WmNews wmNews = getById(id);
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章不存在");
        } else {
            return ResponseResult.okResult(wmNews);
        }
    }

    /**
     * 文章删除
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult deleteNews(Integer id) {
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章id不可缺少");
        }
        WmNews wmNews = getById(id);
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章不存在");
        } else if (wmNews.getStatus().equals(WmNews.Status.PUBLISHED.getCode())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "文章已发布，不能删除");
        } else {
            removeById(id);
            wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery()
                    .eq(WmNewsMaterial::getNewsId,id));
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }
    }

    /**
     * 文章上下架
     *
     * @param wmNewsDto1
     * @return
     */
    @Override
    public ResponseResult downOrUp(WmNewsDto1 wmNewsDto1) {
        // 参数校验
        if (wmNewsDto1.getId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章id不可缺少");
        }
        // 根据id查询文章，查询不到返回错误 ，状态不对也错误
        WmNews wmNews = getById(wmNewsDto1.getId());
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "文章不存在");
        } else if (!wmNews.getStatus().equals(WmNews.Status.PUBLISHED.getCode())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "当前文章不是发布状态，不能上下架");
        } else {
            // 修改上下架
            wmNews.setEnable(wmNewsDto1.getEnable());
            updateById(wmNews);
            // 将文章id和状态发到kafka里面
            if (wmNewsDto1.getId()!=null) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("articleId",wmNews.getArticleId());
                map.put("enable",wmNewsDto1.getEnable());
                kafkaTemplate.send(WmNewsMessageConstants.WM_NEWS_UP_OR_DOWN_TOPIC,JSON.toJSONString(map));
            }
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }
    }

    // 保存文章封面图片与素材的关系
    private void saveRelationInfoForCover(WmNewsDto dto, WmNews wmNews, List<String> images) {
        if (dto.getImages() != null && dto.getImages().size() > 0) {
            saveRelationInfo(wmNews, dto.getImages(), WemediaConstants.WM_COVER_REFERENCE);
        }
        if (dto.getType().equals(WemediaConstants.WM_NEWS_TYPE_AUTO)) {
            if (images.size() >= 3) {
                wmNews.setType(WemediaConstants.WM_NEWS_MANY_IMAGE);
                images = images.stream().limit(3).collect(Collectors.toList());
                wmNews.setImages(StringUtils.join(images, ","));
            } else if (images.size() > 0) {
                wmNews.setType(WemediaConstants.WM_NEWS_SINGLE_IMAGE);
                images = images.stream().limit(1).collect(Collectors.toList());
                wmNews.setImages(StringUtils.join(images, ","));
            } else {
                wmNews.setType(WemediaConstants.WM_NEWS_NONE_IMAGE);
            }
            updateById(wmNews);
            saveRelationInfo(wmNews, images, WemediaConstants.WM_COVER_REFERENCE);
        }
    }

    // 保存文章内容图片与素材的关系
    private void saveRelationInfo(WmNews wmNews, List<String> images, Short type) {
        if (!images.isEmpty()) {
            List<WmMaterial> wmMaterials = wmMaterialMapper.selectList(Wrappers.<WmMaterial>lambdaQuery().in(WmMaterial::getUrl, images));
            if (wmMaterials == null || wmMaterials.size() == 0) {
                throw new CustomException(AppHttpCodeEnum.PARAM_INVALID);
            }
            List<Integer> ids = wmMaterials.stream().map(WmMaterial::getId).collect(Collectors.toList());
            if (images.size() != ids.size()) {
                throw new CustomException(AppHttpCodeEnum.PARAM_INVALID);
            }
            wmNewsMaterialMapper.saveRelations(ids, wmNews.getId(), type);
        }
    }

    // 提取文章内容图片路径
    private List<String> extractUrlInfo(WmNewsDto dto) {
        ArrayList<String> list = new ArrayList<>();
        List<Map> maps = JSON.parseArray(dto.getContent(), Map.class);
        for (Map map : maps) {
            if (map.get("type").equals(WemediaConstants.WM_NEWS_TYPE_IMAGE)) {
                list.add((String) map.get("value"));
            }
        }
        return list;
    }

    // 保存或修改文章
    private void saveOrUpdateInfo(WmNewsDto dto, WmNews wmNews) {
        wmNews.setUserId(WmThreadLocalUtil.getUser().getId());
        wmNews.setCreatedTime(new Date());
        //提交时间 不是发布时间，发布时间前台已传
        wmNews.setSubmitedTime(new Date());
        wmNews.setEnable((short) 1);
        if (dto.getId() == null) {
            save(wmNews);
        } else {
            wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery().eq(WmNewsMaterial::getNewsId, wmNews.getId()));
            updateById(wmNews);
        }
    }
}
