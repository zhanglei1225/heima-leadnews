package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.ApArticleService;
import com.heima.article.service.ArticleFreemarkerService;
import com.heima.common.constants.ArticleConstants;
import com.heima.common.constants.BehaviorConstants;
import com.heima.common.redis.CacheService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.dtos.ArticleInfoDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.article.pojos.ApArticleContent;
import com.heima.model.article.vos.HotArticleVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.ArticleVisitStreamMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.thread.AppThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ClassName: ApArticleServiceImpl
 * Package: com.heima.article.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/24 20:33
 * @Version 11
 */
@Service
@Transactional
@Slf4j
public class ApArticleServiceImpl extends ServiceImpl<ApArticleMapper, ApArticle> implements ApArticleService {

    @Autowired
    private ApArticleMapper apArticleMapper;
    @Autowired
    private ApArticleConfigMapper apArticleConfigMapper;
    @Autowired
    private ApArticleContentMapper apArticleContentMapper;
    @Autowired
    private ArticleFreemarkerService articleFreemarkerService;
    @Autowired
    private CacheService cacheService;

    /**
     * 根据参数加载文章列表
     *
     * @param loadType 1为加载更多 2为加载最新
     * @param dto
     * @return
     */
    @Override
    public ResponseResult load(Short loadType, ArticleHomeDto dto) {
        // 校验参数
        Integer size = dto.getSize();
        if (size ==null || size==0) {
            size =10;
        }
        size =Math.min(size,50);
        dto.setSize(size);
        // 文章频道校验
        if (StringUtils.isEmpty(dto.getTag())) {
            dto.setTag(ArticleConstants.DEFAULT_TAG);
        }
        // 时间校验
        if (dto.getMaxBehotTime()==null) {
            dto.setMaxBehotTime(new Date());
        }
        if (dto.getMinBehotTime()==null) {
            dto.setMinBehotTime(new Date());
        }
        // 查询数据
        List<ApArticle> apArticles = apArticleMapper.loadArticleList(dto, loadType);
        // 结果封装
        return ResponseResult.okResult(apArticles);
    }

    /**
     * 保存app端相关文章
     * @param dto
     * @return
     */
    @Override
    public ResponseResult saveArticle(ArticleDto dto) {
        // 降级测试
        /*try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        // 1.检查参数
        if (dto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        // 2.保存相关文章表
        ApArticle apArticle = new ApArticle();
        BeanUtils.copyProperties(dto,apArticle);
        // 新增
        if (dto.getId()==null) {
            save(apArticle);
            ApArticleConfig apArticleConfig = new ApArticleConfig(apArticle.getId());
            apArticleConfigMapper.insert(apArticleConfig);
            ApArticleContent apArticleContent = new ApArticleContent();
            apArticleContent.setContent(dto.getContent());
            apArticleContent.setArticleId(apArticle.getId());
            apArticleContentMapper.insert(apArticleContent);
        }else {
            // 更新 ap_article   ap_article_content
            updateById(apArticle);
            ApArticleContent apArticleContent = apArticleContentMapper.selectOne(Wrappers.<ApArticleContent>lambdaQuery()
                    .eq(ApArticleContent::getArticleId, apArticle.getId()));
            apArticleContent.setContent(dto.getContent());
            apArticleContentMapper.updateById(apArticleContent);
        }
        //异步调用 生成静态文件上传到minio中
        articleFreemarkerService.buildArticleToMinio(apArticle,dto.getContent());
        // 3.返回article_id
        return ResponseResult.okResult(apArticle.getId());
    }

    /**
     * 加载文章列表
     *
     * @param loadType  1为加载更多 2为加载最新
     * @param dto
     * @param firstPage true 首页  false 非首页
     * @return
     */
    @Override
    public ResponseResult load2(Short loadType, ArticleHomeDto dto, boolean firstPage) {
        if (firstPage) {
            String json = cacheService.get(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + dto.getTag());
            List<HotArticleVo> hotArticleVoList = JSON.parseArray(json, HotArticleVo.class);
            return ResponseResult.okResult(hotArticleVoList);
        }
        return load(loadType,dto);
    }

    /**
     * 加载文章行为
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult loadArticle(ArticleInfoDto dto) {
        // 参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        ApUser user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        boolean isLike =false, isUnlike=false, iscollection=false, isFollow=false;
        Object like = cacheService.hGet(BehaviorConstants.LIKE_BEHAVIOR + dto.getArticleId(), user.getId() + "");
        if (like!=null) {
            isLike=true;
        }
        Object unLike = cacheService.hGet(BehaviorConstants.UNLIKES_BEHAVIOR + dto.getArticleId(), user.getId() + "");
        if (unLike!=null) {
            isUnlike=true;
        }
        Object collection = cacheService.hGet(BehaviorConstants.COLLECTION_BEHAVIOR + dto.getArticleId(), user.getId() + "");
        if (collection!=null) {
            iscollection=true;
        }
        Boolean follow = cacheService.sIsMember(BehaviorConstants.USER_FOLLOW_BEHAVIOR + dto.getAuthorId(), user.getId() + "");
        if (follow) {
            isFollow=true;
        }
        HashMap<String, Boolean> hashMap = new HashMap<>();
        hashMap.put("islike",isLike);
        hashMap.put("isunlike",isUnlike);
        hashMap.put("iscollection",iscollection);
        hashMap.put("isfollow",isFollow);
        return ResponseResult.okResult(hashMap);
    }

    /**
     * 更新文章的分值  同时更新缓存中的热点文章数据
     *
     * @param streamMess
     */
    @Override
    public void updateScore(ArticleVisitStreamMess streamMess) {
        //1.更新文章的阅读、点赞、收藏、评论的数量
        ApArticle apArticle = updateArticle(streamMess);
        //2.计算文章的分值
        Integer score =computeScore(apArticle);
        score =score *3;
        //3.替换当前文章对应频道的热点数据
        replaceDataToRedis(apArticle,score,ArticleConstants.HOT_ARTICLE_FIRST_PAGE
                +apArticle.getChannelId());
        //4.替换推荐对应的热点数据
        replaceDataToRedis(apArticle,score,ArticleConstants.HOT_ARTICLE_FIRST_PAGE
                +ArticleConstants.DEFAULT_TAG);
    }

    /**
     * 替换数据并且存入到redis
     * @param apArticle
     * @param score
     * @param key
     */
    private void replaceDataToRedis(ApArticle apArticle, Integer score, String key) {
        String articleListStr = cacheService.get(key);
        if (StringUtils.isNotBlank(articleListStr)) {
            List<HotArticleVo> hotArticleVoList = JSON.parseArray(articleListStr, HotArticleVo.class);
            boolean flag =true;
            //如果缓存中存在该文章，只更新分值
            for (HotArticleVo hotArticleVo : hotArticleVoList) {
                if (hotArticleVo.getId().equals(apArticle.getId())) {
                    hotArticleVo.setScore(score);
                    flag =false;
                    break;
                }
            }
            //如果缓存中不存在，查询缓存中分值最小的一条数据，进行分值的比较，如果当前文章的分值大于缓存中的数据，就替换
            if (flag) {
                if (hotArticleVoList.size()>=30) {
                    hotArticleVoList=hotArticleVoList.stream().sorted((o1, o2) -> o2.getScore()
                            -o1.getScore()).collect(Collectors.toList());
                    HotArticleVo lastHot = hotArticleVoList.get(hotArticleVoList.size() - 1);
                    if (lastHot.getScore()<score) {
                        HotArticleVo hotArticleVo = new HotArticleVo();
                        BeanUtils.copyProperties(apArticle,hotArticleVo);
                        hotArticleVo.setScore(score);
                        hotArticleVoList.add(hotArticleVo);
                    }
                }else {
                    HotArticleVo hotArticleVo = new HotArticleVo();
                    BeanUtils.copyProperties(apArticle,hotArticleVo);
                    hotArticleVo.setScore(score);
                    hotArticleVoList.add(hotArticleVo);
                }
            }
            //缓存到redis
            hotArticleVoList=hotArticleVoList.stream().sorted((o1, o2) -> o2.getScore()
                    -o1.getScore()).collect(Collectors.toList());
            cacheService.set(key,JSON.toJSONString(hotArticleVoList));
        }
    }

    /**
     * 计算文章的具体分值
     * @param apArticle
     * @return
     */
    private Integer computeScore(ApArticle apArticle) {
        Integer score =0;
        if (apArticle.getViews()!=null) {
            score+=apArticle.getViews();
        }
        if (apArticle.getLikes()!=null) {
            score+= apArticle.getLikes()*ArticleConstants.HOT_ARTICLE_LIKE_WEIGHT;
        }
        if (apArticle.getComment()!=null) {
            score+= apArticle.getComment()*ArticleConstants.HOT_ARTICLE_COMMENT_WEIGHT;
        }
        if (apArticle.getCollection()!=null) {
            score+= apArticle.getCollection()*ArticleConstants.HOT_ARTICLE_COLLECTION_WEIGHT;
        }
        return score;
    }

    /**
     * 更新文章行为数量
     * @param streamMess
     * @return
     */
    private ApArticle updateArticle(ArticleVisitStreamMess streamMess) {
        ApArticle apArticle = getById(streamMess.getArticleId());
        apArticle.setCollection(apArticle.getCollection()==null?streamMess.getCollect()
                :streamMess.getCollect()+apArticle.getCollection());

        apArticle.setComment(apArticle.getComment()==null?streamMess.getComment()
                :streamMess.getComment()+apArticle.getComment());

        apArticle.setLikes(apArticle.getLikes()==null?streamMess.getLike()
                :streamMess.getLike()+apArticle.getLikes());

        apArticle.setViews(apArticle.getViews()==null?streamMess.getView()
                :streamMess.getView()+apArticle.getViews());
        updateById(apArticle);
        return apArticle;
    }
}
