package com.heima.minio.test;

import com.heima.file.service.FileStorageService;
import com.heima.minio.MinIoApplication;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * ClassName: MinIoTest
 * Package: com.heima.minio.test
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/25 16:15
 * @Version 11
 */
@SpringBootTest(classes = MinIoApplication.class)
@RunWith(SpringRunner.class)
public class MinIoTest {
    @Autowired
    private FileStorageService fileStorageService;
    /**
     * 把list.html文件上传到minio中，并且可以在浏览器中访问
     * @param args
     */
    public static void main(String[] args) {
        try {
            FileInputStream fileInputStream = new FileInputStream("C:\\5kqgis\\FileTest\\plugins\\vant\\vue.min.js");
            // 1.获取minio的链接信息 创建一个minio客户端
            MinioClient minioClient = MinioClient.builder().credentials("minio", "minio123")
                    .endpoint("http://192.168.200.130:9000").build();
            // 2.上传
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object("plugins/vant/vue.min.js") // 文件名称
                    .contentType("text/js") //文件类型
                    .bucket("leadnews") //桶名称 与minio管理页面创建的桶一致
                    .stream(fileInputStream, fileInputStream.available(), -1)
                    .build();
            minioClient.putObject(putObjectArgs);
            // 访问路径
            System.out.println("upload success");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Test
    public void testuploadHtmlFile(){
        try {
            FileInputStream fileInputStream = new FileInputStream("C:\\5kqgis\\FileTest\\leadNewsTest\\list.html");
            String filepath = fileStorageService.uploadHtmlFile("", "list.html", fileInputStream);
            System.out.println(filepath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testuploadImgFile(){
        try {
            FileInputStream fileInputStream = new FileInputStream("C:\\Users\\mac\\Pictures\\Saved Pictures\\porn1.jpg");
            String filepath = fileStorageService.uploadImgFile("", "porn1.jpg", fileInputStream);
            System.out.println(filepath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
