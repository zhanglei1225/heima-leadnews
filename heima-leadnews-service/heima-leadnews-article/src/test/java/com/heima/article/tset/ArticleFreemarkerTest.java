package com.heima.article.tset;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.ArticleApplication;
import com.heima.article.service.ApArticleService;
import com.heima.article.service.impl.ApArticleContentServiceImpl;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleContent;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;

/**
 * ClassName: ArticleFreemarkerTest
 * Package: com.heima.article.tset
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/25 20:10
 * @Version 11
 */
@SpringBootTest(classes = ArticleApplication.class)
@RunWith(SpringRunner.class)
public class ArticleFreemarkerTest {

    @Autowired
    private ApArticleService apArticleService;
    @Autowired
    private Configuration configuration;
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private ApArticleContentServiceImpl apArticleContentService;

    @Test
    public void testCreateStaticUrl() throws Exception {
        // 获取文章内容
        List<ApArticleContent> contentList = apArticleContentService.list();
        for (ApArticleContent apArticleContent : contentList) {
            if (apArticleContent !=null && StringUtils.isNotBlank(apArticleContent.getContent())) {
                // 将文章内容通过freemarker生成html文件
                Template template = configuration.getTemplate("article.ftl");
                // 准备模板数据
                HashMap<String, Object> map = new HashMap<>();
                String content = apArticleContent.getContent();
                JSONArray jsonArray = JSONArray.parseArray(content);
                map.put("content",jsonArray);
                StringWriter stringWriter = new StringWriter();
                template.process(map,stringWriter);
                // 将html文件上传到minio
                ByteArrayInputStream inputStream = new ByteArrayInputStream(stringWriter.toString().getBytes());
                String path = fileStorageService.uploadHtmlFile("", apArticleContent.getArticleId() + ".html", inputStream);
                // 修改ap_article表，保存static_url字段
                apArticleService.update(Wrappers.<ApArticle>lambdaUpdate().eq(ApArticle::getId,apArticleContent.getArticleId())
                        .set(ApArticle::getStaticUrl,path));
            }

        }
    }
}
