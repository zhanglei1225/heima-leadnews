package com.heima.search.service.impl;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.pojos.ApAssociateWords;
import com.heima.search.service.ApAssociateWordsService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName: ApAssociateWordsServiceImpl
 * Package: com.heima.search.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/6 15:55
 * @Version 11
 */
@Service
public class ApAssociateWordsServiceImpl implements ApAssociateWordsService {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 联想词
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findAssociate(UserSearchDto dto) {
        //1 参数检查
        if (dto==null || StringUtils.isBlank(dto.getSearchWords())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2 分页检查
        if (dto.getPageSize()>20) {
            dto.setPageSize(20);
        }
        //3 执行查询 模糊查询
        Query query = Query.query(Criteria.where("associateWords").regex(".*" + dto.getSearchWords() + ".*"));
        List<ApAssociateWords> wordsList = mongoTemplate.find(query, ApAssociateWords.class);
        return ResponseResult.okResult(wordsList);
    }
}
