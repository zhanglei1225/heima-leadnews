package com.heima.model.user.dtos;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ClassName: LoginDto
 * Package: com.heima.model.user.dtos
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/21 18:09
 * @Version 11
 */
@Data
public class LoginDto {
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号",required = true)
    private String phone;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码",required = true)
    private String password;
}
