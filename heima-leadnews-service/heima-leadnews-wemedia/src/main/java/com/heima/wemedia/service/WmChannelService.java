package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;

import java.util.List;

/**
 * ClassName: WmChannelService
 * Package: com.heima.wemedia.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/6/27 16:53
 * @Version 11
 */
public interface WmChannelService extends IService<WmChannel> {
    ResponseResult findAll();

    /**
     * 保存频道
     * @param wmChannel
     * @return
     */
    ResponseResult saveChannel(WmChannel wmChannel);

    /**
     * 频道名称模糊分页查询
     * @param dto
     * @return
     */
    ResponseResult listChannels(ChannelDto dto);

    /**
     * 修改频道
     * @param wmChannel
     * @return
     */
    ResponseResult updateChannels(WmChannel wmChannel);

    /**
     * 删除频道
     * @param id
     * @return
     */
    ResponseResult delChannel(Integer id);
}
