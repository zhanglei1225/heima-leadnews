package com.heima.schedule.feign;

import com.heima.apis.schedule.ITaskClient;
import com.heima.model.schedule.dtos.Task;
import com.heima.schedule.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: TaskClient
 * Package: com.heima.schedule.feign
 * Description:
 *
 * @Author Tree
 * @Create 2023/7/2 20:18
 * @Version 11
 */
@RestController
public class TaskClient implements ITaskClient {
    @Autowired
    private TaskService taskService;

    @PostMapping("api/v1/task/addTask")
    public void addTsk(@RequestBody Task task){
        taskService.addTsk(task);
    }

    @GetMapping("api/v1/task/poll/{taskType}/{taskPriority}")
    public Task poll(@PathVariable("taskType") int taskType, @PathVariable("taskPriority") int priority) {
        return taskService.poll(taskType,priority);
    }
}
